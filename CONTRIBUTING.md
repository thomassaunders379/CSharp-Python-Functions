# CSharp Python Functions - Contribution Guide

## Introduction

First off, thank you for considering contributing to CSharp Python Functions. It's people like you that make life easier for people transfering they're skills from Python to C#!

Following these guidelines helps to communicate to the developers managing and developing CSharp Python Functions in a way that is easy for everyone. In return, they should efficiently address your issue, assessing changes, and helping you finalize your pull requests.

## Contributions

### Contributions we're looking for

There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code which can be incorporated into CSharp Python Functions itself.

### Contributions we're not looking for

Again, defining this up front means less work for you. If someone ignores your guide and submits something you don’t want, you can simply close it and point to your policy.

Please, don't use the issue tracker for support questions. Please [email](mailto:incoming+thomassaunders379/CSharp-Python-Functions@incoming.gitlab.com?Subject=Support%20Request) us and we will be happy to help you.

Additions that do not fit the scope of the project (not a python related function) will not be merged into the project, for what we hope are obvious reasons.

### Ground rules for all

Please be welcoming to newcomers and encourage diverse new contributors from all backgrounds. Everyone is commiting they're time for the good of the project.

### Responsibilities for new pull requests

* Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
* Don't add any new files to the codebase unless absolutely needed. Err on the side of using functions. Chances are all your functions will fit into the files already present
* Keep feature versions as small as possible, preferably one new feature per version.

### Your First Contribution

Unsure where to begin contributing to CSharp Python Functions? You can start by looking through these beginner and help-wanted issues:

* Beginner issues - issues which should only require a few lines of code, and a test or two.  These mostly will be bugs
* Help wanted issues - issues which should be a bit more involved than beginner issues.

Once your pull request is accepted, we'll be sure to add you to the CONTRIBUTORS.md list so you can show off your contributions to a project!

#### Guides

Here are a couple of tutorials for you to follow if your new to open source project contribution:

* [Make A Pull Request](http://makeapullrequest.com/)
* [First Timers Only](http://www.firsttimersonly.com/)

At this point, you're ready to make your changes! Feel free to ask for help; everyone is a beginner at first :smile_cat:

If a maintainer asks you to "rebase" your PR, they're saying that a lot of code has changed, and that you need to update your branch so it's easier to merge.

## Getting started

### Submitting a contribution

PLEASE MAKE SURE YOU USE THE DEVELOPMENT BRANCH!

#### For something that is bigger than a one or two line fix

1. Create your own fork of the code
2. Do the changes in your fork
3. If you like the change and think the project could use it:
* Be sure you have followed the code style for the project.
* Send a Pull Request accuratly detailing what changes you have made

### Fixes without new functionality

As a rule of thumb, changes are obvious fixes if they do not introduce any new functionality or creative thinking. As long as the change does not affect functionality, some likely examples include the following:

* Spelling / grammar fixes
* Typo correction, white space and formatting changes
* Comment clean up
* Bug fixes that change default return values or error codes stored in constants
* Adding logging messages or debugging output
* Changes to ‘metadata’ files like Gemfile, .gitignore, build scripts, etc.
* Moving source files from one directory or package to another

These changes do not need to have as much detail as the regular pull requests, just a brief overview of the changes you have made.

## How to report a bug

When filing an issue, make sure to answer these five questions:

* What did you do?
* What did you expect to see?
* What did you see instead?

General questions should be [emailed](mailto:incoming+thomassaunders379/CSharp-Python-Functions@incoming.gitlab.com) to us rather than using the issue tracker.

## How to suggest a feature or enhancement

Update UPCOMMING.md file with your suggested changes, and create a pull request.  Accurate descriptions will also assist in having your idea accepted.

Given the project name, it goes without saying that every new feature suggestion sould be related to a python function.

## Code review process

Who reviews it? Who needs to sign off before it’s accepted? When should a contributor expect to hear from you? How can contributors get commit access, if at all?

The core team looks at Pull Requests on a regular basis and discusses them in full, keeping updates on the pull request discussion page. We will use this to coordinate with the user who filed the pull request and before merging.